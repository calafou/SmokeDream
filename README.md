This visualization software is thought to interact with a CO detection kit with arduino and MQ7 sending the data through Smoke-Ghost (Postgres SQL).
d3.js, path transitions, linear easing, spline interpolation are some of the concepts involved.
Tha base example from Mike Bostock: https://bl.ocks.org/mbostock/4060954